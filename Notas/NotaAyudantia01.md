# Notas de la Ayudantía 09/Marzo/2021

* Sesión en linea (BigBlueButton y opciones)
* Chat: canal ##cienciacomputacion en freenode
* Google Classroom
* Correo
* Vemos que se necesita

# Dudas y ejercicios
* Ejercicios de la sección
  - Ver su propuesta
  - Ahondar o ser necesario ver correcciones
* Ejercicios relacionados
* Dudas en general

* Emily Noether. 
* Nacio el 23 de marzo de 1982 y murió el 14 de abril de 1935.
* Creo el teorema de Noether. 
* Trabajó en Álgebra abstracta: Anillos y Campos.
* Tras doctorarse trabajó en el instituto de Erlangen: 7 años sin paga. 
(1908-1915). 
* Hilbert y Klein la invitan a Göttingen. (1915-1933).

* Fue Moscú (1928-1929) con Pavel Sergeyecich Alexandrov. 
* Fritz Noether (su hermano) trabajó en Sibería. 
* Werner Weber, ex estudiante de Noether, impulsa la purga.
* Bryn Mawr College en USA. Princeton como invitada mal vista.

* Bletcheley Park
En la Segunda Guerra Mundial el papel de la mujer fue muy importante y el 80%
de las 9,000 personas que trabajan en el lugar era mujeres.
1945-1960 tuvieron un papel protagónico.
La industria desestima la labor de la programación hasta 1970.
Sheryl Sandbertg, ex chief operating officer de facebook: inequidades salariales



