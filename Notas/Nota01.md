# Notas de Autómantas Semana 02 8/03/2021
Si Σ es un alfabeto, entonces un lenguaje L es un subconjunto de Σ^*

Nota:
* Dado que Σ es finito, Σ^* es un conjunto infinito del mismo tamaño de N.
* El conjunto de subconjuntos de Σ^* se denota como P(Σ^*).
* Por la definición anterior, el conjunto de lenguajes en Σ^* es P(Σ^*).
* P(Σ^*) es inifito, de un tamaño mayor que el de N.

Si pensamos en los lenguajes como conjuntos, podemos usar las siguientes 
operaciones.

Sean A y B dos lenguajes Σ. Tenemos las siguientes operaciones:
- Union
- Interseccion
- Diferencia
- Complemento
- Concatenación
- Estrella de Kleene
- Tenemos el operador A^+ que genera todas las cadenas.

Una gramatica es una cuarteta
* Un alfabeto de símbolos terminales
* Un alfabeto de símbolos no terminales
* S ∈ Γ es el simbolo inicial
* Reglas de producción

La jerarquía de Chomsky
* Tipo 3: Si todas las reglas de producciones tienen la forma:
A -> \alphaB o A -> \alpha, donde A,B∈Γ y \alpha ∈ Σ^*
* Tipo 2: Si todas las reglas tienen la forma A -> \alpha, donde A ∈ Γ
y \alpha ∈ (Σ U Γ)^* 
* Tipo 1: Si mo hay una regla de producción con la forma \alpha -> ε
* Tipo 0: Sin restricciones

Gramáticas y el curso.

Estos tipos también conocen con otros nombres:
* Tipo 3: Lenguajes regulares
* Tipo 2: Lenguajes independientes del contexto o libres del contexto
* Tipo 1: Lenguajes dependientes del contexto o sensibles al contexto
* Tipo 0: Lenguajes recursivamente enumerables

Estudiaremos los tipos 3,2 y 0 en las primeras tres partes del curso.



